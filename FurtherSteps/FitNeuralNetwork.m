function [net,D_pr] = FitNeuralNetwork(M,D,neurons)
%fits neural network to input data M (all parameters in rows, each patient
%per column) and given binary decision 0: no sickness 1 sickness

%coversion from arrray to output matrix necessary for neural network
D_NN(1:2,1:length(D))=0;

for i=1:length(D)
   if D(i)==0
      D_NN(1,i)=1;
   else
      D_NN(2,i)=1;
   end
end



%specify 'trainbr' for Bayesian regularization may perform better than
%(enter nothing) Scale COnjugate Gradient but takes longer
net=patternnet(neurons);

%Set the division of train/val/test dataset, test: this is the "blinded"
%prospective test. Default values are : 70%,15%,15%
net.divideParam.trainRatio = 70/100;
net.divideParam.valRatio = 15/100;
net.divideParam.testRatio = 15/100;

[net,tr]=train(net,M,D_NN);
D_NN_pr=net(M); %this makes actually no sense now.
%M should be divided into training and test set upfront, to really have a
%comparison (Prospective). Now it is just benchmarking of Algorithm 

%this command extracts the TEST (prospective so to say) from all the data
%Then Dprediction should be calc via Dprediction=net(Mtest) and compared
%with Dtest
% Mtest = M(:,tr.testInd);
% Dtest = D(:,tr.testInd);

plotconfusion(D_NN,D_NN_pr);
c=confusion(D_NN,D_NN_pr)*100;
sprintf(strcat('Quality of prediction :',' ',num2str(100-round(c)),' %%'))


D_NN_pr=round(D_NN_pr);
%Now convert back the NN output matrix to prediction array
D_pr(1:length(D_NN_pr))=0;

    for i=1:2
        for j=1:length(D)
            if D_NN_pr(i,j)==1
            D_pr(j)=i-1;
            end
        end
    end




D_pr=D_pr';
end

