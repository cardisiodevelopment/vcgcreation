function BoxplotParameters(M,D,index)
%Boxplot to visually compare individual parameters M is input Matrix of all
%Parameters. D is input array of Diagnosis, index is the index in the names
%array, which parameter to look at. 
%can also be extracted from significant_indeces from
%StatisticalComparison.m

names={'MDphi','SDphi','MDalphaR','SDalphaR','MDalphaT','SDalphaT','MDbetaR','SDbetaR','MDbetaT','SDbetaT','MDRoct4',...
    'SDRoct4','MDToct5','SDToct5','MDToct6','SDToct6','MeanToctP5','MDSumTbas','SDSumTbas','MDRatioPT','SDRatioPT',...
    'MDSumPMinus','SDSumPMinus','MDalphaRini','SDalphaRini','MeanRvmaxTvmax','MDRintTint','SDRintTint'};

%Extract the Parameters of sick and healthy patients Par is an array with
%values of only healthy (or sick) patients
Par_KHK=M(index,:).*D';
Par_noKHK=M(index,:)-Par_KHK;
Par_KHK=Par_KHK(Par_KHK~=0);
Par_noKHK=Par_noKHK(Par_noKHK~=0);

%Do statistical comparison assuming normal distribution of values and sig.
%lvl of 0.05

%alternative is:
%p=ranksum(Par_KHK,Par_noKHK):
sign_level=0.05;
[h,p]=ttest2(Par_KHK,Par_noKHK,sign_level);
p=round(p*100)/100;

if h==1
ParameterDiag='Difference exists';
else
ParameterDiag='No Difference exists';
end

group = [repmat({'noKHK'}, length(Par_KHK), 1); repmat({'KHK'}, length(Par_noKHK), 1)];
boxplot([Par_noKHK,Par_KHK],group)
title(strcat(names{index},'  p=',num2str(p),'   .',ParameterDiag, '  p_{krit}=', num2str(sign_level)))
end

