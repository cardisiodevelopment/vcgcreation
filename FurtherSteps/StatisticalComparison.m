function [significant_parameters,significance_of_parameters,significant_indeces] = StatisticalComparison(M,D)
%This makes a statistical comparison off all parameters to see if they are
%really relevant for KHK diagnosis. 
%Output gives the names of the significantly different parameters, the 
%significane value of all parameters (bar plot), low = GOOD difference 
%and the indeces in the names array (necessary for BoxplotParameters.m

names={'MDphi','SDphi','MDalphaR','SDalphaR','MDalphaT','SDalphaT','MDbetaR','SDbetaR','MDbetaT','SDbetaT','MDRoct4',...
    'SDRoct4','MDToct5','SDToct5','MDToct6','SDToct6','MeanToctP5','MDSumTbas','SDSumTbas','MDRatioPT','SDRatioPT',...
    'MDSumPMinus','SDSumPMinus','MDalphaRini','SDalphaRini','MeanRvmaxTvmax','MDRintTint','SDRintTint'};
for i=1:28
index=i;
clearvars Par_KHK Par_noKHK
%Extract the Parameters of sick and healthy patients Par is an array with
%values of only healthy (or sick) patients
Par_KHK=M(index,:).*D';
Par_noKHK=M(index,:)-Par_KHK;
Par_KHK=Par_KHK(Par_KHK~=0);
Par_noKHK=Par_noKHK(Par_noKHK~=0);

%Do statistical comparison assuming normal distribution of values and sig.
%lvl of 0.05

%alternative is:
%p=ranksum(Par_KHK,Par_noKHK):
sign_lvl=0.1;
[h,p]=ttest2(Par_KHK,Par_noKHK,sign_lvl);
p=round(p*100)/100;

%calculate Cohen's d to judge the effect size >0.8 or 1 are desired
effect_size(i)=abs((mean(Par_KHK)-mean(Par_noKHK))/sqrt((std(Par_KHK)^2+std(Par_noKHK)^2)/2));

significance_of_parameters(i)=p;
%significant_parameter{1}='';
end
figure(1)
bar(significance_of_parameters);
title('Significance of difference, <0.05 is desired')
xlabel('Parameter N�');
ylabel('p value t-test');
figure(2)
bar(effect_size);
title('Effect size of difference, >0.8 or 1 is desired')
xlabel('Parameter N�');
ylabel('Cohen''s d');
counter=1;

for i=1:28
    if significance_of_parameters(i)<0.1
        significant_parameters{counter}=names{i};
        significant_indeces{counter}=i;
        counter=counter+1;
    end
end
counter=1;

for i=1:28
    if effect_size(i)>0.8
        high_effect_parameters{counter}=names{i};
        high_effect_indeces{counter}=i;
        counter=counter+1;
    end
end
significant_parameters
significant_indeces
high_effect_parameters
high_effect_indeces
end