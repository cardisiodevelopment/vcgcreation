function ExportDataToXLS(PatientName,ExportFolderPath,CGMParameterValues,sanz_names,sanz_act_value,sanz_set_point,sanz_score,sanz_gender)
%Plots computed CGM parameters to excel sheet,
%next to it the information from Sanz Parameter Diagnosis is plotted as
%well.%both is saved in ExcelSpread Sheet (Path)
%Input are all parameters as well as content of Sanz Diagnosis Table and
%Patient Name

names={'MDphi','SDphi','MDalphaR','SDalphaR','MDalphaT','SDalphaT','MDbetaR','SDbetaR','MDbetaT','SDbetaT','MDRoct4',...
    'SDRoct4','MDToct5','SDToct5','MDToct6','SDToct6','MeanToctP5','MDSumTbas','SDSumTbas','MDRatioPT','SDRatioPT',...
    'MDSumPMinus','SDSumPMinus','MDalphaRini','SDalphaRini','MeanRvmaxTvmax','MDRintTint','SDRintTint'}';
for i=1:length(CGMParameterValues)
values{i}=CGMParameterValues(i);
end
values=values';

%Definition of Result matrices
CGMheader={'Parameter','Value'};
CGMmatrix=[names,values];


SANZmatrix=[sanz_names;num2cell(sanz_act_value);sanz_set_point;num2cell(sanz_score)]';
SANZheader={'Parameter','Actual Value','Set Point','Score'};


if sanz_gender==1
SANZgender={'Gender','Male'}';
else
SANZgender={'Gender','Female'};
end

SANZdiagnosis={'Diagnosis',num2str(sum(sanz_score))};

NameHeader={'Name',PatientName};

%Write the data the Excel Sheet
filename = strcat(ExportFolderPath,'\',PatientName,'.xlsx');
xlswrite(filename,NameHeader,1,'E1')
xlswrite(filename,CGMheader,1,'A3')
xlswrite(filename,CGMmatrix,1,'A5')
xlswrite(filename,SANZheader,1,'E3')
xlswrite(filename,SANZmatrix,1,'E5')
xlswrite(filename,SANZdiagnosis,1,'G18')
xlswrite(filename,SANZgender,1,'J3')
end

