function varargout = VCG_GUI(varargin)
% VCG_GUI MATLAB code for VCG_GUI.fig
%      VCG_GUI, by itself, creates a new VCG_GUI or raises the existing
%      singleton*.
%
%      H = VCG_GUI returns the handle to a new VCG_GUI or the handle to
%      the existing singleton*.
%
%      VCG_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VCG_GUI.M with the given input arguments.
%
%      VCG_GUI('Property','Value',...) creates a new VCG_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before VCG_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to VCG_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help VCG_GUI

% Last Modified by GUIDE v2.5 16-Aug-2016 13:39:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @VCG_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @VCG_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before VCG_GUI is made visible.
function VCG_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to VCG_GUI (see VARARGIN)

% Choose default command line output for VCG_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
evalin( 'base', 'clear all' )
LoadInitialValues();
set(handles.AutomaticEvaluation,'Enable','off');
set(handles.SelectDataAutomatic,'Enable','off');
set(handles.ExportFolderAutomaticEvaluation,'Enable','off');
set(handles.figure1,'Toolbar','figure'); %insert toolbar to allow zooming and rotating
axes(handles.axes9)
matlabImage = imread('logo.png');
image(matlabImage)
axis off
axis image
%pushbutton11_Callback(hObject, eventdata, handles);

% UIWAIT makes VCG_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = VCG_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in RespirationRemoval.
function RespirationRemoval_Callback(hObject, eventdata, handles)
% hObject    handle to RespirationRemoval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if evalin('base','exist(''X1'')')==0
 msgbox('First execute plot CGM durchführen','Error','error')
else
X1=evalin('base','X1');
Y1=evalin('base','Y1');
Z1=evalin('base','Z1');
time=evalin('base','time');
end

fs=evalin('base','MeasFreq');
RespFreq=evalin('base','RespFreq');
X1filt=FIRhighpass(X1,RespFreq,fs);
Y1filt=FIRhighpass(Y1,RespFreq,fs);
Z1filt=FIRhighpass(Z1,RespFreq,fs);


axes(handles.axes1)
plot3(X1,Y1,Z1,X1filt,Y1filt,Z1filt);
% Create xlabel
xlabel('X [mV]');
% Create ylabel
ylabel('Y [mV]');
% Create zlabel
zlabel('Z [mV]');
% Create title
title({'CGM'});


axes(handles.axes2)
graphTitle='X Lead';
lineColor='red';
plotECG(time,X1filt,graphTitle,lineColor,fs);

axes(handles.axes3)
graphTitle='Y Lead';
lineColor='red';
plotECG(time,Y1filt,graphTitle,lineColor,fs);
hold on

axes(handles.axes4)
graphTitle='Z Lead';
lineColor='red';
plotECG(time,Z1filt,graphTitle,lineColor,fs);
hold on




assignin('base','X1filt',X1filt); 
assignin('base','Y1filt',Y1filt); 
assignin('base','Z1filt',Z1filt); 






% --- Executes on button press in SavitzkyGolay.
function SavitzkyGolay_Callback(hObject, eventdata, handles)
% hObject    handle to SavitzkyGolay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if evalin('base','exist(''X1filt'')')==0
 msgbox('First execute: plot CGM and Respiration Removal','Error','error')
else
X1filt=evalin('base','X1filt');
Y1filt=evalin('base','Y1filt');
Z1filt=evalin('base','Z1filt');
time=evalin('base','time');
end

fs=evalin('base','MeasFreq');
X1=evalin('base','X1');
Y1=evalin('base','Y1');
Z1=evalin('base','Z1');


X1sgolay=savitzkygolay(X1filt);
Y1sgolay=savitzkygolay(Y1filt);
Z1sgolay=savitzkygolay(Z1filt);

N=length(time);
X1sgolay=X1sgolay(ceil(N/10):ceil(N-N/10)); %take jsut the middle 80% of the measurement series
Y1sgolay=Y1sgolay(ceil(N/10):ceil(N-N/10));
Z1sgolay=Z1sgolay(ceil(N/10):ceil(N-N/10));
time=time(ceil(N/10):ceil(N-N/10))-time(ceil(N/10));

axes(handles.axes1)
plot3(X1,Y1,Z1,X1filt,Y1filt,Z1filt,X1sgolay,Y1sgolay,Z1sgolay);
% Create xlabel
xlabel('X [mV]');
% Create ylabel
ylabel('Y [mV]');
% Create zlabel
zlabel('Z [mV]');
% Create title
title({'CGM'});


axes(handles.axes2)
graphTitle='X Lead';
lineColor='yellow';
plotECG(time,X1sgolay,graphTitle,lineColor,fs);

axes(handles.axes3)
graphTitle='Y Lead';
lineColor='yellow';
plotECG(time,Y1sgolay,graphTitle,lineColor,fs);
hold on

axes(handles.axes4)
graphTitle='Z Lead';
lineColor='yellow';
plotECG(time,Z1sgolay,graphTitle,lineColor,fs);
hold on



assignin('base','X1sgolay',X1sgolay); 
assignin('base','Y1sgolay',Y1sgolay); 
assignin('base','Z1sgolay',Z1sgolay); 
assignin('base','time',time); 



% --- Executes on button press in ClearFigures.
function ClearFigures_Callback(hObject, eventdata, handles)
% hObject    handle to ClearFigures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1)
cla
axes(handles.axes2)
cla
axes(handles.axes3)
cla
axes(handles.axes4)
cla
axes(handles.axes5)
cla

axes(handles.axes8)
cla


set(handles.DiagnosisTable,'Data',[]);

set(handles.QQ,'string','ms')   
set(handles.SDQQ,'string','SD')   
set(handles.RR,'string','ms')   
set(handles.SDRR,'string','SD')
set(handles.SS,'string','ms')   
set(handles.SDSS,'string','SD')
set(handles.TT,'string','ms')   
set(handles.SDTT,'string','SD') 
set(handles.PP,'string','ms')   
set(handles.SDPP,'string','SD') 

set(handles.RLoopDur,'string','ms')   
set(handles.RLoopSD,'string','SD') 
set(handles.TLoopDur,'string','ms')   
set(handles.TLoopSD,'string','SD')
set(handles.PLoopDur,'string','ms')   
set(handles.PLoopSD,'string','SD')

set(handles.phi,'string','°')   
set(handles.SDphi,'string','SD')  
set(handles.alphaR,'string','°')   
set(handles.SDalphaR,'string','SD') 
set(handles.alphaT,'string','°')   
set(handles.SDalphaT,'string','SD')  
set(handles.betaR,'string','°')   
set(handles.SDbetaR,'string','SD')   
set(handles.betaT,'string','°')   
set(handles.SDbetaT,'string','SD')  

set(handles.Roct4,'string','%')   
set(handles.SDRoct4,'string','SD')  
set(handles.Toct5,'string','%')  
set(handles.SDToct5,'string','SD')  
set(handles.Toct6,'string','%') 
set(handles.SDToct6,'string','SD') 
set(handles.SumTbas,'string','%') 
set(handles.SDSumTbas,'string','SD') 

set(handles.RatioPT,'string','X') 
set(handles.SDRatioPT,'string','SD') 

set(handles.SumPMinus,'string','mVms') 
set(handles.SDSumPMinus,'string','mVms')

set(handles.MeanTOctP5,'string','mV')

set(handles.alphaRini,'string','°')   
set(handles.SDalphaRini,'string','SD') 

set(handles.MeanRvmaxTvmax,'string', 'X') 

set(handles.RintTint,'string','X')   
set(handles.SDRintTint,'string','SD') 

set(handles.diagnosis,'string','')

evalin( 'base', 'clearvars -except PathName ExportFolderPath FileName_list FileName' )
LoadInitialValues();



function RespFreqBox_Callback(hObject, eventdata, handles)
% hObject    handle to RespFreqBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RespFreqBox as text
%        str2double(get(hObject,'String')) returns contents of RespFreqBox as a double

RespFreq=str2double(get(hObject,'String'));
assignin('base','RespFreq',RespFreq); 

% --- Executes during object creation, after setting all properties.
function RespFreqBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RespFreqBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotcleansignal.
function plotcleansignal_Callback(hObject, eventdata, handles)
% hObject    handle to plotcleansignal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1)
cla
axes(handles.axes2)
cla
axes(handles.axes3)
cla
axes(handles.axes4)
cla

if evalin('base','exist(''X1sgolay'')')==0
 msgbox('First execute: plot CGM, Respiration Removal and Savitzky Golay Filtering','Error','error')
else
X1sgolay=evalin('base','X1sgolay');
Y1sgolay=evalin('base','Y1sgolay');
Z1sgolay=evalin('base','Z1sgolay');
time=evalin('base','time');
end




fs=evalin('base','MeasFreq');
VCG=sqrt(X1sgolay.^2+Y1sgolay.^2+Z1sgolay.^2);

axes(handles.axes1)
plotCGMData(X1sgolay,Y1sgolay,Z1sgolay);

hold on
axes(handles.axes2)
graphTitle='X Lead';
lineColor='blue';
plotECG(time,X1sgolay,graphTitle,lineColor,fs);

axes(handles.axes3)
graphTitle='Y Lead';
lineColor='blue';
plotECG(time,Y1sgolay,graphTitle,lineColor,fs);
hold on

axes(handles.axes4)
graphTitle='Z Lead';
lineColor='blue';
plotECG(time,Z1sgolay,graphTitle,lineColor,fs);
hold on

axes(handles.axes5)
graphTitle='VCG(t)';
lineColor='blue';
plotECG(time,VCG,graphTitle,lineColor,fs);
hold on

 


% --- Executes on button press in QRSTP.
function QRSTP_Callback(hObject, eventdata, handles)
% hObject    handle to QRSTP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if evalin('base','exist(''X1sgolay'')')==0
 msgbox('First execute: plot CGM, Respiration Removal and Savitzky Golay Filtering','Error','error')
else
    X1sgolay=evalin('base','X1sgolay');
    Y1sgolay=evalin('base','Y1sgolay');
    Z1sgolay=evalin('base','Z1sgolay');
    time=evalin('base','time');
end
   
  
    fs=evalin('base','MeasFreq');             % Sampling rate
   
    t = time;        % time index

    %QRS detection algorithm for vy and vcg
    x1=X1sgolay; %hipass and sgolay filtered signal, "raw data"

    y1=Y1sgolay;
    [y_qrs, y_der_sq]=QRSdetection_filtering(y1);
    [Qy,Ry,Sy]=QRSdetection(y_qrs,y1,fs);

    z1=Z1sgolay;

    vcg=sqrt(x1.^2+y1.^2+z1.^2); %VCG from leads
    
    [vcg_qrs, vcg_der_sq]=QRSdetection_filtering(vcg);
    [Qvcg,Rvcg,Svcg]=QRSdetection(vcg_qrs,vcg,fs);
   


    %Compute QRS begin and end from VCG data and Qy Sy detected peaks
    QRS_begin=QWaveBeginDetection(vcg_der_sq,Qy);
    QRS_end=SWaveEndDetection(vcg_der_sq,Sy);
    RR=ceil(mean([RRInterval(Ry), RRInterval(Rvcg)])); %computes Heart rate from two RR Intervals (Ry peak and Rvcg peak distance)
    
    
    %Computes the peak of TWave from vcg data 
     
    [Tvcg] = TDetectionFromVCG(vcg,Rvcg,QRS_begin,QRS_end,RR);
    
    vcg_der_sq_noQRS=RemoveQRSFromSignal(vcg_der_sq,QRS_begin,QRS_end,RR);
    vcg_der_sq_noQRS=vcg_der_sq_noQRS/abs(max(vcg_der_sq_noQRS));

    Tvcgbegin=QWaveBeginDetection(vcg_der_sq_noQRS,Tvcg);
    Tvcgend=SWaveEndDetection(vcg_der_sq_noQRS,Tvcg);
    Tvcg=ReduceMultPeakFromR(Rvcg,Tvcg); %Remove multiple T peaks between two R peaks
    
    %All T peaks and Tbegin/end are taken from VCG, all R are taken from
    %VCG
    T_begin=Tvcgbegin;
    T_end=Tvcgend;
    T=Tvcg;
    R=Rvcg;
    
    
    [Py,y1_der_sq_noQRST] = Pdetection( y1,QRS_begin,T_end ); %P peak detection using  y leads.
    Py=ReduceMultPeakFromR(Ry,Py);
    P_begin=QWaveBeginDetection(y1_der_sq_noQRST,Py); %beginning of Pwave
    P_end=SWaveEndDetection(y1_der_sq_noQRST,Py); %end of Pwave
    P=Py;
    
    %Compute phase durations and SD
    [ QRS_Phase_Duration, QRS_Phase_SD] = ComputePhaseDuration(QRS_begin, QRS_end,fs);
    [ T_Phase_Duration, T_Phase_SD] = ComputeTPhaseDuration(T_begin, T_end,fs);
    [ P_Phase_Duration, P_Phase_SD] = ComputeTPhaseDuration(P_begin, P_end,fs);
    
    %Just check if QQ RR SS TT PP distance is always the same (approx)
    %otherwise this gives a clue what might be wrong. E.g. if PP is very
    %different from the others than P detection did not work properly OR
    %there is pathology in P wave
    [QQ,SD_QQ]=ComputePeakDetQuality(Qy,fs);
    [RR,SD_RR]=ComputePeakDetQuality(R,fs);
    [SS,SD_SS]=ComputePeakDetQuality(Sy,fs);
    [TT,SD_TT]=ComputePeakDetQuality(T,fs);
    [PP,SD_PP]=ComputePeakDetQuality(P,fs);
    
    %This is to get same amount of detected R and T waves, QRS begin and
    %QRS end, Tbegin and Tend, Pbegin and Pend entries.
    minLength=min([length(R) length(T)]);
    R=R(1:minLength);
    T=T(1:minLength);
    minLength=min([length(QRS_begin) length(QRS_end)]);
    QRS_begin=QRS_begin(1:minLength);
    QRS_end=QRS_end(1:minLength);
    minLength=min([length(T_begin) length(T_end)]);
    T_begin=T_begin(1:minLength);
    T_end=T_end(1:minLength);
    minLength=min([length(P_begin) length(P_end)]);
    P_begin=P_begin(1:minLength);
    P_end=P_end(1:minLength);


    %Copes entries in Workspace, more for development/debugging. Can be removed
    %afterwards. This part, from here
    assignin('base','R',R);
    assignin('base','T',T);
    assignin('base','P',P);
    assignin('base','QRS_begin',QRS_begin);
    assignin('base','QRS_end',QRS_end);
    assignin('base','T_begin',T_begin);
    assignin('base','T_end',T_end);
    assignin('base','P_begin',P_begin);
    assignin('base','P_end',P_end);

   %%%%%%Until here, is not necessary for working code.



axes(handles.axes3)
graphTitle='Y Lead';
plotECG_and_Intervals(t,y1,Ry,Sy,Qy,T,fs); 
hold on
plot(t(P),y1(P),'ks');
hold on

axes(handles.axes5)
graphTitle='VCG(t)';
plotVCG_and_Intervals(t,vcg,R,T,P,QRS_begin,QRS_end,T_begin,T_end,P_begin,P_end,fs); 
hold on



%Computation of CGM Parameters
[ MDphi, SDphi ] = ComputePhi( R,T,vcg,x1,y1,z1 );
[ MDalphaR, SDalphaR ] = ComputeAlpha(R,x1,z1);
[ MDalphaT, SDalphaT ] = ComputeAlpha(T,x1,z1);
[ MDbetaR, SDbetaR ] = ComputeBeta(R,x1,y1);
[ MDbetaT, SDbetaT ] = ComputeBeta(T,x1,y1);

[MDRoct4,SDRoct4]=ComputeLoopInOct( x1,y1,z1,QRS_begin,QRS_end,4 );
 [MDToct5,SDToct5,MeanToctP5]=ComputeLoopInOct( x1,y1,z1,T_begin,T_end,5 );   
 [MDToct6,SDToct6]=ComputeLoopInOct( x1,y1,z1,T_begin,T_end,6 );
 [MDSumTbas,SDSumTbas]=ComputeLoopInOct( x1,y1,z1,T_begin,T_end,5678);
 [ MDRatioPT, SDRatioPT ] = ComputeRatioPT(vcg,P,T);
 [ MDSumPMinus,SDSumPMinus ] = ComputeSumPMinus( vcg,P,P_end,fs );
 
 [ MDalphaRini, SDalphaRini ] = ComputeAlpha(QRS_begin+10*fs/1000,x1,z1);
 [ MeanRvmaxTvmax ] = ComputeRvmaxTvmax( vcg,QRS_begin,QRS_end,T_begin,T_end,fs );
 [ MDRintTint, SDRintTint ] = ComputeRintTint( vcg,QRS_begin,QRS_end,T_begin,T_end,fs );

axes(handles.axes1)
plot3(x1(R),y1(R),z1(R),'rs',x1(T),y1(T),z1(T),'b^',x1(P),y1(P),z1(P),'ks','LineWidth',2);
hold on

set(handles.axes8,'visible','on');
axes(handles.axes8)
matlabImage = imread('legend.png');
image(matlabImage)
axis off
axis image

%Write all parameters to Workspace
assignin('base','MDphi',MDphi);
assignin('base','SDphi',SDphi);
assignin('base','MDalphaR',MDalphaR);
assignin('base','SDalphaR',SDalphaR);
assignin('base','MDalphaT',MDalphaT);
assignin('base','SDalphaT',SDalphaT);
assignin('base','MDbetaR',MDbetaR);
assignin('base','SDbetaR',SDbetaR);
assignin('base','MDbetaT',MDbetaT);
assignin('base','SDbetaT',SDbetaT);
assignin('base','MDRoct4',MDRoct4);
assignin('base','SDRoct4',SDRoct4);
assignin('base','MDToct5',MDToct5);
assignin('base','SDToct5',SDToct5);
assignin('base','MDToct6',MDToct6);
assignin('base','SDToct6',SDToct6);
assignin('base','MeanToctP5',MeanToctP5);
assignin('base','MDSumTbas',MDSumTbas);
assignin('base','SDSumTbas',SDSumTbas);
assignin('base','MDRatioPT',MDRatioPT);
assignin('base','SDRatioPT',SDRatioPT);
assignin('base','MDSumPMinus',MDSumPMinus);
assignin('base','SDSumPMinus',SDSumPMinus);
assignin('base','MDalphaRini',MDalphaRini);
assignin('base','SDalphaRini',SDalphaRini);
assignin('base','MeanRvmaxTvmax',MeanRvmaxTvmax);
assignin('base','MDRintTint',MDRintTint);
assignin('base','SDRintTint',SDRintTint);

%Write all parameters into Matrix (necessary to create AllCGMData in one
%Excel sheet
CGMParameterValues(1)=MDphi;
CGMParameterValues(2)=SDphi;
CGMParameterValues(3)=MDalphaR;
CGMParameterValues(4)=SDalphaR;
CGMParameterValues(5)=MDalphaT;
CGMParameterValues(6)=SDalphaT;
CGMParameterValues(7)=MDbetaR;
CGMParameterValues(8)=SDbetaR;
CGMParameterValues(9)=MDbetaT;
CGMParameterValues(10)=SDbetaT;
CGMParameterValues(11)=MDRoct4;
CGMParameterValues(12)=SDRoct4;
CGMParameterValues(13)=MDToct5;
CGMParameterValues(14)=SDToct5;
CGMParameterValues(15)=MDToct6;
CGMParameterValues(16)=SDToct6;
CGMParameterValues(17)=MeanToctP5;
CGMParameterValues(18)=MDSumTbas;
CGMParameterValues(19)=SDSumTbas;
CGMParameterValues(20)=MDRatioPT;
CGMParameterValues(21)=SDRatioPT;
CGMParameterValues(22)=MDSumPMinus;
CGMParameterValues(23)=SDSumPMinus;
CGMParameterValues(24)=MDalphaRini;
CGMParameterValues(25)=SDalphaRini;
CGMParameterValues(26)=MeanRvmaxTvmax;
CGMParameterValues(27)=MDRintTint;
CGMParameterValues(28)=SDRintTint;

assignin('base','CGMParameterValues',CGMParameterValues);



set(handles.QQ,'string',strcat(num2str(QQ),' ms'))   
set(handles.SDQQ,'string',strcat(num2str(SD_QQ),' ms'))   
set(handles.RR,'string',strcat(num2str(RR), 'ms'))   
set(handles.SDRR,'string',strcat(num2str(SD_RR), ' ms'))
set(handles.SS,'string',strcat(num2str(SS),' ms'))   
set(handles.SDSS,'string',strcat(num2str(SD_SS), ' ms'))
set(handles.TT,'string',strcat(num2str(TT),' ms'))   
set(handles.SDTT,'string',strcat(num2str(SD_TT), ' ms')) 
set(handles.PP,'string',strcat(num2str(PP), ' ms'))   
set(handles.SDPP,'string',strcat(num2str(SD_PP), ' ms')) 

set(handles.RLoopDur,'string',strcat(num2str(QRS_Phase_Duration), ' ms'))   
set(handles.RLoopSD,'string',strcat(num2str(QRS_Phase_SD),' ms')) 
set(handles.TLoopDur,'string',strcat(num2str(T_Phase_Duration), ' ms'))   
set(handles.TLoopSD,'string',strcat(num2str(T_Phase_SD),' ms')) 
set(handles.PLoopDur,'string',strcat(num2str(P_Phase_Duration),' ms'))   
set(handles.PLoopSD,'string',strcat(num2str(P_Phase_SD),' ms')) 

set(handles.phi,'string',strcat(num2str(MDphi),' °'))   
set(handles.SDphi,'string',strcat(num2str(SDphi),' °'))  
set(handles.alphaR,'string',strcat(num2str(MDalphaR), ' °'))   
set(handles.SDalphaR,'string',strcat(num2str(SDalphaR), ' °')) 
set(handles.alphaT,'string',strcat(num2str(MDalphaT), ' °'))   
set(handles.SDalphaT,'string',strcat(num2str(SDalphaT),' °'))  
set(handles.betaR,'string',strcat(num2str(MDbetaR),' °'))   
set(handles.SDbetaR,'string',strcat(num2str(SDbetaR), ' °'))   
set(handles.betaT,'string',strcat(num2str(MDbetaT), ' °'))   
set(handles.SDbetaT,'string',strcat(num2str(SDbetaT), ' °'))  

set(handles.Roct4,'string',strcat(num2str(MDRoct4), ' %'))  
set(handles.SDRoct4,'string',strcat(num2str(SDRoct4), ' %'))  
set(handles.Toct5,'string',strcat(num2str(MDToct5), ' %')) 
set(handles.SDToct5,'string',strcat(num2str(SDToct5), ' %'))
set(handles.Toct6,'string',strcat(num2str(MDToct6),' %'))  
set(handles.SDToct6,'string',strcat(num2str(SDToct6),' %'))
set(handles.SumTbas,'string',strcat(num2str(MDSumTbas),' %')) 
set(handles.SDSumTbas,'string',strcat(num2str(SDSumTbas),' %'))

set(handles.RatioPT,'string',strcat(num2str(MDRatioPT),'')) 
set(handles.SDRatioPT,'string',strcat(num2str(SDRatioPT),''))
set(handles.SumPMinus,'string',strcat(num2str(MDSumPMinus),' mVms')) 
set(handles.SDSumPMinus,'string',strcat(num2str(SDSumPMinus),' mVms'))
set(handles.MeanTOctP5,'string',strcat(num2str(MeanToctP5),' mV'))

set(handles.alphaRini,'string',strcat(num2str(MDalphaRini), ' °'))   
set(handles.SDalphaRini,'string',strcat(num2str(SDalphaRini), ' °')) 

set(handles.MeanRvmaxTvmax,'string',strcat(num2str(MeanRvmaxTvmax), '')) 
set(handles.RintTint,'string',strcat(num2str(MDRintTint),'')) 
set(handles.SDRintTint,'string',strcat(num2str(SDRintTint),''))


function QQ_Callback(hObject, eventdata, handles)
% hObject    handle to QQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of QQ as text
%        str2double(get(hObject,'String')) returns contents of QQ as a double


% --- Executes during object creation, after setting all properties.
function QQ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to QQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RR_Callback(hObject, eventdata, handles)
% hObject    handle to RR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RR as text
%        str2double(get(hObject,'String')) returns contents of RR as a double


% --- Executes during object creation, after setting all properties.
function RR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SS_Callback(hObject, eventdata, handles)
% hObject    handle to SS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SS as text
%        str2double(get(hObject,'String')) returns contents of SS as a double


% --- Executes during object creation, after setting all properties.
function SS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TT_Callback(hObject, eventdata, handles)
% hObject    handle to TT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TT as text
%        str2double(get(hObject,'String')) returns contents of TT as a double


% --- Executes during object creation, after setting all properties.
function TT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SDRR_Callback(hObject, eventdata, handles)
% hObject    handle to SDRR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SDRR as text
%        str2double(get(hObject,'String')) returns contents of SDRR as a double


% --- Executes during object creation, after setting all properties.
function SDRR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SDRR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SDSS_Callback(hObject, eventdata, handles)
% hObject    handle to SDSS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SDSS as text
%        str2double(get(hObject,'String')) returns contents of SDSS as a double


% --- Executes during object creation, after setting all properties.
function SDSS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SDSS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SDTT_Callback(hObject, eventdata, handles)
% hObject    handle to SDTT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SDTT as text
%        str2double(get(hObject,'String')) returns contents of SDTT as a double


% --- Executes during object creation, after setting all properties.
function SDTT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SDTT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SDQQ_Callback(hObject, eventdata, handles)
% hObject    handle to SDQQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SDQQ as text
%        str2double(get(hObject,'String')) returns contents of SDQQ as a double


% --- Executes during object creation, after setting all properties.
function SDQQ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SDQQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RLoopDur_Callback(hObject, eventdata, handles)
% hObject    handle to RLoopDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RLoopDur as text
%        str2double(get(hObject,'String')) returns contents of RLoopDur as a double


% --- Executes during object creation, after setting all properties.
function RLoopDur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RLoopDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TLoopDur_Callback(hObject, eventdata, handles)
% hObject    handle to TLoopDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TLoopDur as text
%        str2double(get(hObject,'String')) returns contents of TLoopDur as a double


% --- Executes during object creation, after setting all properties.
function TLoopDur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TLoopDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TLoopSD_Callback(hObject, eventdata, handles)
% hObject    handle to TLoopSD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TLoopSD as text
%        str2double(get(hObject,'String')) returns contents of TLoopSD as a double


% --- Executes during object creation, after setting all properties.
function TLoopSD_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TLoopSD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RLoopSD_Callback(hObject, eventdata, handles)
% hObject    handle to RLoopSD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RLoopSD as text
%        str2double(get(hObject,'String')) returns contents of RLoopSD as a double


% --- Executes during object creation, after setting all properties.
function RLoopSD_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RLoopSD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% 

% --- Executes on button press in plotCGM.
function plotCGM_Callback(hObject, eventdata, handles)
% hObject    handle to plotCGM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Evaluate Path and File Name
PathName=evalin('base','PathName');
FileName=evalin('base','FileName');

[time,H0,A,Ve,D,I]=LoadCGMData(PathName,FileName);

[ X1,Y1,Z1 ] = CreateCGMLeads( H0,A,Ve,D,I );

PatientName=FileName(1:length(FileName)-4);
fs=500;
% Create VCG
axes(handles.axes1)
plotCGMData(X1,Y1,Z1);

set(handles.PatientenName,'string',PatientName) 

%create ECG of X Y Z leads
axes(handles.axes2)
cla
graphTitle='X Lead';
lineColor='blue';
plotECG(time,X1,graphTitle,lineColor,fs);
hold on


axes(handles.axes3);
cla %clears axes
graphTitle='Y Lead';
lineColor='blue';
plotECG(time,Y1,graphTitle,lineColor,fs);
hold on

axes(handles.axes4)
cla
graphTitle='Z Lead';
lineColor='blue';
plotECG(time,Z1,graphTitle,lineColor,fs);
hold on

linkaxes([handles.axes2,handles.axes3,handles.axes4,handles.axes5],'x');

assignin('base','X1',X1); 
assignin('base','Y1',Y1); 
assignin('base','Z1',Z1); 
assignin('base','time',time);
assignin('base','PatientName',PatientName);



function MeasFreq_Callback(hObject, eventdata, handles)
% hObject    handle to MeasFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MeasFreq as text
%        str2double(get(hObject,'String')) returns contents of MeasFreq as a double
MeasFreq=str2double(get(hObject,'String'));
assignin('base','MeasFreq',MeasFreq); 

% --- Executes during object creation, after setting all properties.
function MeasFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MeasFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotextraCGM.
function plotextraCGM_Callback(hObject, eventdata, handles)
% hObject    handle to plotextraCGM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
X1sgolay=evalin('base','X1sgolay');
Y1sgolay=evalin('base','Y1sgolay');
Z1sgolay=evalin('base','Z1sgolay');
R=evalin('base','R');
T=evalin('base','T');
P=evalin('base','P');
createDetailedCGM(X1sgolay,Y1sgolay,Z1sgolay,X1sgolay(R),Y1sgolay(R),Z1sgolay(R),X1sgolay(T),Y1sgolay(T),Z1sgolay(T),X1sgolay(P),Y1sgolay(P),Z1sgolay(P));


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%Evaluate all parameters from Workspace

SDphi=evalin('base','SDphi');
MDalphaR=evalin('base','MDalphaR');
MDalphaT=evalin('base','MDalphaT');
MDbetaR=evalin('base','MDbetaR');
SDbetaR=evalin('base','SDbetaR');
MDbetaT=evalin('base','MDbetaT');
MDRoct4=evalin('base','MDRoct4');
MDToct5=evalin('base','MDToct5');
MDToct6=evalin('base','MDToct6');
MeanToctP5=evalin('base','MeanToctP5');
MDSumTbas=evalin('base','MDSumTbas');
SDRatioPT=evalin('base','SDRatioPT');
MDSumPMinus=evalin('base','MDSumPMinus');
MDalphaRini=evalin('base','MDalphaRini');
MeanRvmaxTvmax=evalin('base','MeanRvmaxTvmax');


GenderValue=get(handles.MaleButton,'Value');
if GenderValue==1
    rnames={'MDbetaR','SDbetaR','MDalphaT','MDbetaT','SDphi','SDPmax/Tmax','MDRoct4','MDToct5','MDToct6','MDSumTbas','MDalphaRini'};
    set_point={'>=-38°','<=2.15°','10°-130°','-56°-1°','<=5.2°','0.09','<=51%','<=1.25%','<=11%','<=39%','<-130° v >45°'};
    act_value=[MDbetaR,SDbetaR,MDalphaT,MDbetaT,SDphi,SDRatioPT,MDRoct4,MDToct5,MDToct6,MDSumTbas,MDalphaRini];
   
    [score]=ComputeSanzScore(act_value,GenderValue);
    set(handles.DiagnosisTable, 'data', [act_value(1),set_point(1),score(1);act_value(2),set_point(2),score(2);act_value(3),set_point(3),score(3);act_value(4),set_point(4),score(4);...
    act_value(5),set_point(5),score(5);act_value(6),set_point(6),score(6);act_value(7),set_point(7),score(7);act_value(8),set_point(8),score(8);act_value(9),set_point(9),score(9);...
    act_value(10),set_point(10),score(10);act_value(11),set_point(11),score(11)],'RowName',rnames);
else
    
    rnames={'RvmaxTvmax','MDalphaR','MDalphaT','MDbetaT','MDToct5','MDSumPMinus','MDSumTbas','MDalphaRini','MeanToctP5'};
    set_point={'<=22.5','<=90°','<=113°','<=1°','<=19°','<=19.6mVms','<=95%','<-126° v >56°','<=0.1mV'};
    act_value=[MeanRvmaxTvmax,MDalphaR,MDalphaT,MDbetaT,MDToct5,MDSumPMinus,MDSumTbas,MDalphaRini,MeanToctP5];
    [score]=ComputeSanzScore(act_value,GenderValue);
    set(handles.DiagnosisTable, 'data', [act_value(1),set_point(1),score(1);act_value(2),set_point(2),score(2);act_value(3),set_point(3),score(3);act_value(4),set_point(4),score(4);...
    act_value(5),set_point(5),score(5);act_value(6),set_point(6),score(6);act_value(7),set_point(7),score(7);act_value(8),set_point(8),score(8);act_value(9),set_point(9),score(9)],'RowName',rnames);
end

t=handles.DiagnosisTable;
t.Position(3) = t.Extent(3);

set(handles.diagnosis,'string',strcat(num2str(sum(score)),'')) 
if sum(score)<0
set(handles.diagnosis,'foregroundcolor',[1 0 0]) 
else
set(handles.diagnosis,'foregroundcolor',[0 0.5 0]) 
end
assignin('base','sanz_act_value',act_value);
assignin('base','sanz_set_point',set_point);
assignin('base','sanz_score',score);
assignin('base','sanz_names',rnames);
assignin('base','sanz_gender',GenderValue);



% --- Executes on button press in MaleButton.
function MaleButton_Callback(hObject, eventdata, handles)
% hObject    handle to MaleButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of MaleButton
set(handles.FemaleButton,'Value',0);
evaluation=evalin( 'base', 'exist(''SDphi'')' ); %check if there is data to evaluate or just gender change
if evaluation==1
pushbutton11_Callback(hObject, eventdata, handles);
end


% --- Executes on button press in FemaleButton.
function FemaleButton_Callback(hObject, eventdata, handles)
% hObject    handle to FemaleButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of FemaleButton
set(handles.MaleButton,'Value',0);
evaluation=evalin( 'base', 'exist(''SDphi'')' ); %check if there is data to evaluate or just gender change
if evaluation==1
pushbutton11_Callback(hObject, eventdata, handles);
end


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ParameterExplanation();


% --- Executes on button press in ExportParameters.
function ExportParameters_Callback(hObject, eventdata, handles)
% hObject    handle to ExportParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CGMParameterValues=evalin('base','CGMParameterValues');
sanz_names=evalin('base','sanz_names');
sanz_act_value=evalin('base','sanz_act_value');
sanz_set_point=evalin('base','sanz_set_point');
sanz_score=evalin('base','sanz_score');
PatientName=evalin('base','PatientName');
sanz_gender=evalin('base','sanz_gender');
ExportFolderPath=evalin('base','ExportFolderPath');


ExportDataToXLS(PatientName,ExportFolderPath,CGMParameterValues,sanz_names,sanz_act_value,sanz_set_point,sanz_score,sanz_gender);


% --- Executes on button press in SelectData.
function SelectData_Callback(hObject, eventdata, handles)
% hObject    handle to SelectData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName] = uigetfile('*.txt','Select the Data txt file');
assignin('base','FileName',FileName);
assignin('base','PathName',PathName);
plotCGM_Callback(hObject, eventdata, handles);


% --- Executes on button press in SelectExportFolder.
function SelectExportFolder_Callback(hObject, eventdata, handles)
% hObject    handle to SelectExportFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ExportFolderPath=uigetdir('','Select Export Folder');
assignin('base','ExportFolderPath',ExportFolderPath);


% --- Executes on button press in AutomaticEvaluation.
function AutomaticEvaluation_Callback(hObject, eventdata, handles)
% hObject    handle to AutomaticEvaluation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

FileName_list=evalin('base','FileName_list');
%PathName=evalin('base','PathName');
%ExportFolderPath=evalin('base','ExportFolderPath');
for i=1:length(FileName_list)
    FileName=FileName_list{i};
    assignin('base','FileName',FileName);
    plotCGM_Callback(hObject, eventdata, handles);
    RespirationRemoval_Callback(hObject, eventdata, handles);
    SavitzkyGolay_Callback(hObject, eventdata, handles);
    plotcleansignal_Callback(hObject, eventdata, handles);
    QRSTP_Callback(hObject, eventdata, handles);
    pushbutton11_Callback(hObject, eventdata, handles);
    ExportParameters_Callback(hObject, eventdata, handles);
    
    CGMParameterValues=evalin('base','CGMParameterValues');
    
    CGMParameterMatrix(i,1:length(CGMParameterValues))=CGMParameterValues;
    ClearFigures_Callback(hObject, eventdata, handles);
    if i==length(FileName_list)
    ExportFolderPath=evalin('base','ExportFolderPath');
    ExportCGMParamInOneTable(ExportFolderPath,CGMParameterMatrix,FileName_list)
    end
end


% --- Executes on button press in SelectDataAutomatic.
function SelectDataAutomatic_Callback(hObject, eventdata, handles)
% hObject    handle to SelectDataAutomatic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName_list,PathName] = uigetfile('*.txt','Select the Data txt file','MultiSelect', 'on');
if (class(FileName_list)=='char')
 msgbox('Please select more than one data file','Error','error')
end

assignin('base','FileName_list',FileName_list);
assignin('base','PathName',PathName);


% --- Executes on button press in ExportFolderAutomaticEvaluation.
function ExportFolderAutomaticEvaluation_Callback(hObject, eventdata, handles)
% hObject    handle to ExportFolderAutomaticEvaluation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ExportFolderPath=uigetdir('','Select Export Folder');
assignin('base','ExportFolderPath',ExportFolderPath);


% --- Executes on button press in ManualEvalCheck.
function ManualEvalCheck_Callback(hObject, eventdata, handles)
% hObject    handle to ManualEvalCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ManualEvalCheck
set(handles.AutomaticEvalCheck,'Value',0);
ClearFigures_Callback(hObject, eventdata, handles);
%set visibility of buttons on/off, so that only necessary buttons are shown
set(handles.plotCGM,'Enable','on');
set(handles.RespirationRemoval,'Enable','on');
set(handles.SavitzkyGolay,'Enable','on');
set(handles.ClearFigures,'Enable','on');
set(handles.plotcleansignal,'Enable','on');
set(handles.QRSTP,'Enable','on');
set(handles.pushbutton11,'Enable','on');
set(handles.pushbutton12,'Enable','on');
set(handles.ExportParameters,'Enable','on');
set(handles.SelectData,'Enable','on');
set(handles.SelectExportFolder,'Enable','on');
set(handles.plotextraCGM,'Enable','on');

set(handles.AutomaticEvaluation,'Enable','off');
set(handles.SelectDataAutomatic,'Enable','off');
set(handles.ExportFolderAutomaticEvaluation,'Enable','off');


% --- Executes on button press in AutomaticEvalCheck.
function AutomaticEvalCheck_Callback(hObject, eventdata, handles)
% hObject    handle to AutomaticEvalCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AutomaticEvalCheck
set(handles.ManualEvalCheck,'Value',0);
ClearFigures_Callback(hObject, eventdata, handles);
%set visibility of buttons on/off, so that only necessary buttons are shown
set(handles.plotCGM,'Enable','off');
set(handles.RespirationRemoval,'Enable','off');
set(handles.SavitzkyGolay,'Enable','off');
set(handles.ClearFigures,'Enable','off');
set(handles.plotcleansignal,'Enable','off');
set(handles.QRSTP,'Enable','off');
set(handles.pushbutton11,'Enable','off');
set(handles.pushbutton12,'Enable','off');
set(handles.ExportParameters,'Enable','off');
set(handles.SelectData,'Enable','off');
set(handles.SelectExportFolder,'Enable','off');
set(handles.plotextraCGM,'Enable','off');

set(handles.AutomaticEvaluation,'Enable','on');
set(handles.SelectDataAutomatic,'Enable','on');
set(handles.ExportFolderAutomaticEvaluation,'Enable','on');
