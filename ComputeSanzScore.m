function [ score ] = ComputeSanzScore(act_value,GenderValue);
%computes the score for each entry according to Sanz et al 2008.
score(1:length(act_value))=0;

if GenderValue==1 %Male boundary values
    if act_value(1)<-38
    score(1)=-1;
    end
    if act_value(2)>2.15
    score(2)=-1;
    end
    if act_value(3)<10||act_value(3)>130
    score(3)=-1;
    end
    if act_value(4)<-56||act_value(4)>1
    score(4)=-1;
    end
    if act_value(5)>5.2
    score(5)=-1;
    end
    if act_value(6)>0.09
    score(6)=-1;
    end
    if act_value(7)>51
    score(7)=-1;
    end
    if act_value(8)>1.25
    score(8)=-1;
    end
    if act_value(9)>11
    score(9)=-1;
    end
    if act_value(10)>39
    score(10)=-1;
    end
    if act_value(11)<45&&act_value(11)>-130
    score(11)=-1;
    end
else %Female boundary values
    if act_value(1)>22.5
    score(1)=-1;
    end
    if act_value(2)>90
    score(2)=-1;
    end
    if act_value(3)>113
    score(3)=-1;
    end
    if act_value(4)>1
    score(4)=-1;
    end
    if act_value(5)>19
    score(5)=-1;
    end
    if act_value(6)>19.6
    score(6)=-1;
    end
    if act_value(7)>95
    score(7)=-1;
    end
    if act_value(8)<56&&act_value(8)>-126
    score(8)=-1;
    end
    if act_value(9)>0.1
    score(9)=-1;
    end
end


end

