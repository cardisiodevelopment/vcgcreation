function [time, H0, A, Ve, D, I] = LoadCGMData(PathName,FileName)
%Reads data from Tobis Input file, timestamp and base64 measurements,
%does the necessary steps to convert into time (in seconds) and
%measurements of the Nehb leads, channels 2,3,4,5,6 ->H0 A Ve D I
%leads are measured in 1 Bit = 19.87 nV,
%output is in mV X*19.87*1e-6;

clear time H0 A Ve D I data_hex

fid=fopen(strcat(PathName,FileName));
data=textscan(fid,'%f %s','delimiter','','headerlines',3); %opens txt file and reads data into cell array
fclose(fid);
[time,data64]=deal(data{:}); %splits the cell array into two colums: 1. is time 2. is base64 measurements
fs=500; %Measurement frequency in Hz
final_time=60*fs;


time=time-time(1);
time=time/(1000000);
time=time(1:final_time);

%initialize the arrays for mV extraction (different leads)
 H0(1:final_time)=0;
 A(1:final_time)=0;
 Ve(1:final_time)=0;
 D(1:final_time)=0;
 I(1:final_time)=0;

for i=1:final_time;
    data64{i}=data64{i}(1:32); %cuts away last entries of cell content, now only measurements exist in base64 notation
    data_hex=base64decode(data64{i}); %delivers one line of hex number taken from base64 data
 
    %extract and convert from hex 2 dec to get the leads
    H0(i)=sscanf(data_hex(7:12),'%x');
    if H0(i)<=8388607
    H0(i)=H0(i);
    else
    H0(i)=H0(i)-16777216;
    end

    A(i)=sscanf(data_hex(13:18),'%x');
    if A(i)<=8388607
    A(i)=A(i);
    else
    A(i)=A(i)-16777216;
    end

    Ve(i)=sscanf(data_hex(19:24),'%x');
    if Ve(i)<=8388607
    Ve(i)=Ve(i);
    else
    Ve(i)=Ve(i)-16777216;
    end

    D(i)=sscanf(data_hex(25:30),'%x');
    if D(i)<=8388607
    D(i)=D(i);
    else
    D(i)=D(i)-16777216;
    end

    I(i)=sscanf(data_hex(31:36),'%x');
    if I(i)<=8388607
    I(i)=I(i);
    else
    I(i)=I(i)-16777216;
    end

end
H0=H0*39.74*1e-6;
A=A*39.74*1e-6;
Ve=Ve*39.74*1e-6;
D=D*39.74*1e-6;
I=I*39.74*1e-6;

% plot(time,H0);
% title('H0');
% figure(2)
% plot(time,A);
% title('A');
% figure(3)
% plot(time,Ve);
% title('Ve');
% figure(4)
% plot(time,D);
% title('D');
% figure(5)
% plot(time,I);
% title('I');

end



