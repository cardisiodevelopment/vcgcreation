function [ CGM_X,CGM_Y,CGM_Z ] = CreateCGMLeads( H0,A,Ve,D,I )
%creates orthogonal CGM leads from measured H0,A,Ve,D,I according to
%formula of Sanz
time=linspace(0,20,10000);
CGM_X=0.7*(D)-(I);
CGM_Y=0.7*(D)+(A);
CGM_Z=0.7*((Ve)-(H0));

CGM_X=CGM_X';
CGM_Y=CGM_Y';
CGM_Z=CGM_Z';

% plot(time,CGM_X);
% figure(2)
% plot(time,CGM_Y);
% figure(3)
% plot(time,CGM_Z);

end

