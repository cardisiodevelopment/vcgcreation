function [ signal_sgolay ] = savitzkygolay(signal)
%Savitzky Golay filter (moving average) to smooth out high frequency
%oscillations of signal

signal_sgolay=sgolayfilt(signal,5,25); %5 is the order of the fitting polynom, 25 is the number of datapoints which are averaged

%signal_sgolay=signal;
end

