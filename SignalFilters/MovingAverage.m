function [ x_filt ] = MovingAverage( x_sq )
% Make impulse response
N = length (x_sq); 
h = ones (1 ,101)/101;
Delay = 50; % Delay in samples

% Apply filter Moving Average
x_ave = conv (x_sq ,h);
x_ave = x_ave (Delay+[1: N]);
x_ave(1:ceil(N/10))=0;
x_ave(N-ceil(N/10):N)=0;
x_ave = x_ave/ max( abs(x_ave ));

x_filt=x_ave;


end

