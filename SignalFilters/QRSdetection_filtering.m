function [x_filt, x_der_sq, x_der]  = QRSdetection_filtering( x,Q,T )
%Derivation, Squaring and Moving Window Integration
%x_filt is the finished filtered signal, meaning: 1. derive+square+moving
%average, x_der_sq is the squared 1. derivation.
N = length (x);       % Signal length



% Make impulse response
h = [-1 -2 0 2 1]/8;
% Apply filter Derivation
x_der = conv (x ,h);
x_der = x_der (2+[1: N]);
x_der(1:ceil(N/10))=0;
x_der(N-ceil(N/10):N)=0;
x_der = x_der/ max( abs(x_der ));

%Signal Squaring
x_sq = x_der .^2;
x_sq = x_sq/ max( abs(x_sq ));
x_der_sq=x_sq;

% Make impulse response
h = ones (1 ,201)/201;
Delay = 100; % Delay in samples

% Apply filter Moving Average
x_ave = conv (x_sq ,h);
x_ave = x_ave (Delay+[1: N]);
x_ave(1:ceil(N/10))=0;  %remove problems (too high values) at beginning and end of data
x_ave(N-ceil(N/10):N)=0;
x_ave = x_ave/ max( abs(x_ave ));

x_filt=x_ave;



end

