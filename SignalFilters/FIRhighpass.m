function [ signal_hipass ] = FIRhighpass( signal, fcutoff,fsample )
%Applies high-pass FIR filter to filter out 0.5 Hz frequencies (baseline
%wander) fcutoff=e.g. 0.5 Hz, fsmaple = sampling rate . e.g. 1000

fnyq=fsample/2; %nyquist frequency
fcutoff_norm=fcutoff/fnyq; 
ahi=fir1(20000,fcutoff_norm,'high'); %FIrst value is the number of taps (the higher the more exact the filter), 0.001 is 0.5 Hz/500Hz(Nyquist freq)
D = mean(grpdelay(ahi));
x1filthidelay = filter(ahi,1,[signal; zeros(D,1)]);

signal_hipass=x1filthidelay(D:length(x1filthidelay)-1);
end

