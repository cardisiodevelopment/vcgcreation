function varargout = ParameterExplanation(varargin)
% PARAMETEREXPLANATION MATLAB code for ParameterExplanation.fig
%      PARAMETEREXPLANATION, by itself, creates a new PARAMETEREXPLANATION or raises the existing
%      singleton*.
%
%      H = PARAMETEREXPLANATION returns the handle to a new PARAMETEREXPLANATION or the handle to
%      the existing singleton*.
%
%      PARAMETEREXPLANATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PARAMETEREXPLANATION.M with the given input arguments.
%
%      PARAMETEREXPLANATION('Property','Value',...) creates a new PARAMETEREXPLANATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ParameterExplanation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ParameterExplanation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ParameterExplanation

% Last Modified by GUIDE v2.5 15-Aug-2016 16:53:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ParameterExplanation_OpeningFcn, ...
                   'gui_OutputFcn',  @ParameterExplanation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ParameterExplanation is made visible.
function ParameterExplanation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ParameterExplanation (see VARARGIN)

% Choose default command line output for ParameterExplanation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ParameterExplanation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ParameterExplanation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Close.
function Close_Callback(hObject, eventdata, handles)
% hObject    handle to Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(gcbf)