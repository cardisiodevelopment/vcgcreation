function [ MDpercentage_in_oct,SDpercentage_in_oct,MeanTOctP5 ] = ComputeLoopInOct( x,y,z,Loop_begin,Loop_end,Octant )
%function calculates the median percentage of RLoop or Tloop in given Octant (basal,apical
%etc) for given QRSbegin and end boundaries (represent the entries in ECG
%signal)

minLength=min([length(Loop_begin),length(Loop_end)]);%make sure begin and end have the same number of entries
Loop_begin=Loop_begin(1:minLength);
Loop_end=Loop_end(1:minLength);

TPot_counter=1; %counter to write out T potential which is in Oct5
MeanTOctP5=0;

for i=1:length(Loop_begin) %loops through QRSbegin/end entries
    vector_interval_x=x(Loop_begin(i):Loop_end(i)); %copies entries from QRS_i/T_i interval to vector array.
    vector_interval_y=y(Loop_begin(i):Loop_end(i));
    vector_interval_z=z(Loop_begin(i):Loop_end(i));
    for j=1:length(vector_interval_x)
        alpha_interval(j)=ComputeAlphaForLoop( vector_interval_x(j),vector_interval_z(j) );
        beta_interval(j)=ComputeBetaForLoop( vector_interval_x(j),vector_interval_y(j) );
        if Octant==5
            if alpha_interval(j)>=-90&&alpha_interval(j)<=0&&beta_interval(j)>=0&&beta_interval(j)<=90;
                MeanTOctP5(TPot_counter)=sqrt(vector_interval_x(j)^2+vector_interval_y(j)^2+vector_interval_z(j)^2);
                TPot_counter=TPot_counter+1;
            end
        end
    end
percentage_in_oct(i)=ComputeAnglePercentage(alpha_interval,beta_interval,Octant);
clearvars alpha_interval beta_interval

end
MDpercentage_in_oct=round((median(percentage_in_oct))*100)/100;
SDpercentage_in_oct=round((std(percentage_in_oct))*100)/100;
MeanTOctP5=round(mean(MeanTOctP5)*100)/100;
end

