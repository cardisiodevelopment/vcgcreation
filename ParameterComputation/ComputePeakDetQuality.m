function [ RR, SD_RR ] = ComputePeakDetQuality( Rx,fs)
%computes peak detection quality by calculated the mean and SD value of peak to peak
 %distance: Low SD means good and consistens detection quality

[RR, SD_RR]=RRInterval(Rx);
RR=ceil(RR*1000/fs);
SD_RR=ceil(SD_RR*1000/fs);
end

