function [ MDSumPMinus,SDSumPMinus ] = ComputeSumPMinus( vcg,P,P_end,fs )
%function calculates the median/SD of temporal integral of potential of
%P-Loop after maximum Pmax in mVms
%signal)

minLength=min([length(P),length(P_end)]);%make sure begin and end have the same number of entries
P=P(1:minLength);
P_end=P_end(1:minLength);

for i=1:minLength %loops through Pmax/end entries
    vcg_interval=vcg(P(i):P_end(i)); %copies entries from QRS_i interval to vector array.
   
SumPMinus(i)=sum(vcg_interval)*1000/fs; %integral of signal
clearvars vcg_interval

end
MDSumPMinus=round((median(SumPMinus))*100)/100;
SDSumPMinus=round((std(SumPMinus))*100)/100;
end

