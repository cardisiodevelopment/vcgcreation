function [ MeanRvmaxTvmax ] = ComputeRvmaxTvmax( vcg,QRS_begin,QRS_end,T_begin,T_end,fs )
%function calculates the mean value of ratio of velocity (derivative) of
%vectors in Rloop and Tloop

minLength=min([length(QRS_begin),length(QRS_end),length(T_begin),length(T_end)]);%make sure begin and end have the same number of entries
QRS_begin=QRS_begin(1:minLength);
QRS_end=QRS_end(1:minLength);
T_begin=T_begin(1:minLength);
T_end=T_end(1:minLength);

for i=1:minLength %loops through QRS_begin/end, Tbegin_end entries
    vcg_interval_QRS=vcg(QRS_begin(i):QRS_end(i)); %copies entries from QRS_i interval to vector array.
    vcg_interval_T=vcg(T_begin(i):T_end(i));
    for j=2:length(vcg_interval_QRS)-1
    diff_vcg_interval_QRS(j)=(vcg_interval_QRS(j+1)-vcg_interval_QRS(j-1))/2*(1000/fs); %compute velocity in mV/ms with central difference diff.
    end
    for j=2:length(vcg_interval_T)-1
    diff_vcg_interval_T(j)=(vcg_interval_T(j+1)-vcg_interval_T(j-1))/2*(1000/fs); %compute velocity in mV/ms
    end
    
Ratio_RvmaxTvmax(i)=max(abs(diff_vcg_interval_QRS))/max(abs(diff_vcg_interval_T));

% clearvars vcg_interval_QRS vcg_interval_T diff_vcg_interval_QRS diff_vcg_interval_T

end
MeanRvmaxTvmax=round(mean(Ratio_RvmaxTvmax)*10)/10;
end