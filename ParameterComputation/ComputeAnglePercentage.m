function [ percentage_in_oct ] = ComputeAnglePercentage( alpha,beta,octant )
%checks if alpha(i) and beta(i) satisfy the criterion that vector is in
%prescribed octant (4,5,6 whatever)
%in the end the percentage related to all angles is given
oct_ok(1:length(alpha))=0;

for i=1:length(alpha)
    if octant==4 %apical superior anterior O4
        if alpha(i)>=90&&alpha(i)<=180&&beta(i)<=0&&beta(i)>=-90;
            oct_ok(i)=1; %if condition is fulfilled switch from 0 to 1
        end
    end
    
    if octant==5 %basal inferior posterior  O5
        if alpha(i)>=-90&&alpha(i)<=0&&beta(i)>=0&&beta(i)<=90;
            oct_ok(i)=1; %if condition is fulfilled switch from 0 to 1
        end
    end
    
     if octant==6 %basal inferior anterior O6
        if alpha(i)>=-180&&alpha(i)<=-90&&beta(i)>=0&&beta(i)<=90;
            oct_ok(i)=1; %if condition is fulfilled switch from 0 to 1
        end
     end
    
      if octant==5678 %basal hemisphere
        if alpha(i)>=-90&&alpha(i)<=0&&beta(i)>=0&&beta(i)<=90;  %O5 & O 7
            oct_ok(i)=1; %if condition is fulfilled switch from 0 to 1
        end
        if alpha(i)>=-180&&alpha(i)<=-90 %O6 and O8
            oct_ok(i)=1; %if condition is fulfilled switch from 0 to 1
        end
       
      
      end  
end
percentage_in_oct=100*sum(oct_ok==1)/length(alpha); %count number of '1' entries and claculate the precentage

end

