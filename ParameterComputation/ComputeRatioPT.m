function [ MDPmaxTmax, SDPmaxTmax ] = ComputeRatioPT(vcg,P,T)
%computes MD and SD of Ratio of Pmax and Tmax

for i=1:min([length(P) length(T)]);
Ratio(i)=vcg(P(i))/vcg(T(i));
end

MDPmaxTmax=round((median(Ratio))*100)/100;
SDPmaxTmax=round((std(Ratio))*100)/100;
end

