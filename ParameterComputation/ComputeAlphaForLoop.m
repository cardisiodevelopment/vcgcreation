function [alpha] = ComputeAlphaForLoop( x1,z1 )
%Computes Alpha of arbitrary vector x1-z1 according to formula
%of Sanz 2008 et al. in x-z plane 


    if (x1>0)&&(z1>0) %0:90� (apical), posterior 
        alpha=rad2deg(atan(z1/x1));
    end
     if (x1<0)&&(z1>0) %90:180� (apical), anterior 
        alpha=rad2deg(atan(z1/x1))+180;
     end
       if (x1>0)&&(z1<0) %0:-90� (apical) posterior 
        alpha=rad2deg(atan(z1/x1));
    end
     if (x1<0)&&(z1<0) %-90:-180 (apical,) anterior 
        alpha=rad2deg(atan(z1/x1))-180;
     end
   



end

