function [ Phase_Duration, Phase_SD ] = ComputePhaseDuration( Ax_begin, Ax_end,fs)
%Computes median phase duration (P T QRS) as median taken from VCG and x,y,z
%determines which of the leads has the least SD
%and gives output as begin and end array of entries!


if Ax_end(1)>Ax_begin(1)
    durx=Ax_end-Ax_begin;
else
    durx=Ax_end(2:length(Ax_end))-Ax_begin(1:length(Ax_begin)-1);
end


Phase_SD=ceil(std(durx)*1000/fs); %compute SD 



Phase_Duration=ceil(mean(durx)*1000/fs);


end

