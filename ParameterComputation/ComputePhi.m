function [ MDphi, SDphi ] = ComputePhi( R,T,vcg,x1,y1,z1 )
%Computes median and SD of R and T vector
%These are the Rmax and Tmax according to the VCG


phi=acos((x1(R).*x1(T)+y1(R).*y1(T)+z1(R).*z1(T))./(vcg(R).*vcg(T)));
MDphi=round(rad2deg(median(phi))*100)/100;
SDphi=round(rad2deg(std(phi))*100)/100;


end

