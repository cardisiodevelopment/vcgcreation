function [beta] = ComputeBetaForLoop( x1,y1 )
%Computes Beta of arbitrary vector x1-y1 according to formula
%of Sanz 2008 et al. in x-y plane 


    if (x1>0)&&(y1>0) %0:90�  inferior
        beta=rad2deg(atan(y1/x1));
    end
     if (x1<0)&&(y1>0) %0:90� inferioir
        beta=rad2deg(atan(y1/x1))+90;
     end
       if (x1>0)&&(y1<0) %0:-90�  superior 
        beta=rad2deg(atan(y1/x1));
    end
     if (x1<0)&&(y1<0) %0:-90 superior 
        beta=rad2deg(atan(y1/x1))-90;
     end
   



end
