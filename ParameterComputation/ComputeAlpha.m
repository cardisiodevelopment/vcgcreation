function [ MDalpha, SDalpha] = ComputeAlpha( R,x1,z1 )
%Computes median and SD of Alpha (For Tmax and Rmax) according to formula
%of Sanz 2008 et al. in x-z plane 

for i=1:length(R)
   
    if (x1(R(i))>0)&&(z1(R(i))>0) %0:90� (apical), posterior 
        alpha(i)=rad2deg(atan(z1(R(i))/x1(R(i))));
    end
     if (x1(R(i))<0)&&(z1(R(i))>0) %90:180� (apical), anterior 
        alpha(i)=rad2deg(atan(z1(R(i))/x1(R(i))))+180;
     end
       if (x1(R(i))>0)&&(z1(R(i))<0) %0:-90� (apical) posterior 
        alpha(i)=rad2deg(atan(z1(R(i))/x1(R(i))));
    end
     if (x1(R(i))<0)&&(z1(R(i))<0) %-90:-180 (apical,) anterior 
        alpha(i)=rad2deg(atan(z1(R(i))/x1(R(i))))-180;
     end

end


MDalpha=round((median(alpha))*100)/100;
SDalpha=round((std(alpha))*100)/100;


end

