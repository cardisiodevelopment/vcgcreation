function [ Phase_Duration, Phase_SD] = ComputeTPhaseDuration( T_begin, T_end,fs)
%Computes median phase duration (P T QRS) as median taken from VCG and x,y,z
%determines which of the leads has the least SD
%and gives output as begin and end array of entries!


if T_end(1)>T_begin(1)
    durx=T_end-T_begin;
else
    durx=T_end(2:length(T_end))-T_begin(1:length(T_begin)-1);
end

Phase_SD=std(durx); %compute SD of all three leads

Phase_SD=ceil(Phase_SD*1000/fs);

Phase_Duration=median(durx);

Phase_Duration=ceil(Phase_Duration*1000/fs);


end

