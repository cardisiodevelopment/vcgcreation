function [ MDRintTint, SDRintTint ] = ComputeRintTint( vcg,QRS_begin,QRS_end,T_begin,T_end,fs )
%function calculates the mean value of ratio of velocity (derivative) of
%vectors in Rloop and Tloop

minLength=min([length(QRS_begin),length(QRS_end),length(T_begin),length(T_end)]);%make sure begin and end have the same number of entries
QRS_begin=QRS_begin(1:minLength);
QRS_end=QRS_end(1:minLength);
T_begin=T_begin(1:minLength);
T_end=T_end(1:minLength);

for i=1:minLength %loops through QRS_begin/end, Tbegin_end entries
    vcg_interval_QRS=vcg(QRS_begin(i):QRS_end(i)); %copies entries from QRS_i interval to vector array.
    vcg_interval_T=vcg(T_begin(i):T_end(i));
    int_vcg_interval_QRS=sum(vcg_interval_QRS)*1000/fs; %integral of QRS signal mVms
    int_vcg_interval_T=sum(vcg_interval_T)*1000/fs; %integral of T signal mVms
    
Ratio_RintTint(i)=int_vcg_interval_QRS/int_vcg_interval_T;
clearvars vcg_interval_QRS vcg_interval_T 

end
MDRintTint=round(median(Ratio_RintTint)*10)/10;
SDRintTint=round(std(Ratio_RintTint)*10)/10;
end