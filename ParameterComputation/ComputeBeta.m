function [ MDbeta, SDbeta ] = ComputeBeta( R,x1,y1 )
%Computes median and SD of Beta(For Tmax and Rmax) according to formula
%of Sanz 2008 et al. in x-y plane 

for i=1:length(R)
    if (x1(R(i))>0)&&(y1(R(i))>0) %0:90�  inferior
        alpha(i)=rad2deg(atan(y1(R(i))/x1(R(i))));
    end
     if (x1(R(i))<0)&&(y1(R(i))>0) %0:90� inferioir
        alpha(i)=rad2deg(atan(y1(R(i))/x1(R(i))))+90;
     end
       if (x1(R(i))>0)&&(y1(R(i))<0) %0:-90�  superior
        alpha(i)=rad2deg(atan(y1(R(i))/x1(R(i))));
    end
     if (x1(R(i))<0)&&(y1(R(i))<0) %0:-90 superior 
        alpha(i)=rad2deg(atan(y1(R(i))/x1(R(i))))-90;
     end
   

end


MDbeta=round((median(alpha))*100)/100;
SDbeta=round((std(alpha))*100)/100;


end

