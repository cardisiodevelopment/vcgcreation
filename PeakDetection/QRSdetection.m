function [ Q_loc R_loc S_loc ] = QRSdetection( x_qrs, x1,fs )

%Find QRS Points , fs is sampling frequency
%Pan-Tompkins algorithm


max_h = max(x_qrs);
thresh = mean (x_qrs );
poss_reg =(x_qrs>thresh*max_h)';
    
%%% Adaptive Thresholding, right not necessary. Check out later maybe
% thresh_ok=0;
% thresh_scaling_factor=1;
% 
% 
% while thresh_ok ~= 1
%     max_h = max(x_qrs);
%     thresh = mean (x_qrs );
%     poss_reg =(x_qrs>thresh*max_h*thresh_scaling_factor)'; %1 or 0 is analyzed data bigger or smaller than threshold
%     
%     for i=1:ceil(fs*3) %a value of poss_reg=1 => possible peak should occur in the first 2 seconds, otherwise lower the threshold
%         if poss_reg(i)==1
%             thresh_ok=1;
%             break
%         else
%             thresh_scaling_factor=thresh_scaling_factor*0.6;
%             if thresh_scaling_factor<0.005
%                 thresh_ok=1;
%             end
%             break
%         end
%     end
% end

left = find(diff([0 poss_reg])==1); %finds entries with difference==1, meaning at this spot there is the jump form 0 to 1
right = find(diff([poss_reg 0])==-1);% finds entries with the jump from 1 to 0


R_loc(1:length(left))=0; %initialisation of R,Q, S location and value
R_value(1:length(left))=0;


Q_value(1:length(left))=0;
Q_loc(1:length(left))=0;
S_value(1:length(left))=0;
S_loc(1:length(left))=0;

for i=1:length(left)

    [R_value(i), R_loc(i)] = max( x1(left(i):right(i)) ); %looks at window left(i)-right(i) e.g. entry 800:900, R_loc goes from 1 to 100
    R_loc(i) = R_loc(i)-1+left(i); % add offset 

    [Q_value(i), Q_loc(i)] = min( x1(left(i):R_loc(i)) );
    Q_loc(i) = Q_loc(i)-1+left(i); % add offset

    [S_value(i), S_loc(i)] = min( x1(R_loc(i):right(i)) );
    S_loc(i) = S_loc(i)-1+R_loc(i); % add offset

end

% there is no selective wave
Q_loc=Q_loc(find(Q_loc~=0));
R_loc=R_loc(find(R_loc~=0));
S_loc=S_loc(find(S_loc~=0));







end

