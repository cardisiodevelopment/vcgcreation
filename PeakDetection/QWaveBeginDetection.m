function [ Qwave_begin ] = QWaveBeginDetection( x5,Q_loc )
% %Check beginning of Q wave
%looks at a squared slope (x5) which is lower than a given threshold,
%Q_loc includes the locations of Q peaks

Qwave_begin(1:length(Q_loc))=0;
Q_loc=Q_loc-20*500/1000;
for i=1:length(Q_loc)
    threshold_qrs=0.01;
    j=0;
    check_flat=0;
    while check_flat==0
        if j>30
            threshold_qrs=threshold_qrs*1.5; %if distance Q - begin Qwave >30 ms, increase threshold start from new
            j=0;
        end
        lowerbound=Q_loc(i)-j-10;
        upperbound=Q_loc(i)-j;
     
        if lowerbound<1
            Qwave_begin(i)=1;
            break;
        else
            check_flat=(sum(x5(lowerbound:upperbound))<threshold_qrs); %sum over 10 points, starting from Q loc going to the left, stop when 
            j=j+1;                                                     %value is lower than threshold. This is beginning of Qwave
            if check_flat==1
                Qwave_begin(i)=Q_loc(i)-j;
            end 
        end
       
    end
end
end

