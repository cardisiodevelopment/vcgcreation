function [ RR,SD_RR] = RRInterval( R )
%computes heart rate (RR peak interval) based on given R peak data
%calculates intervals between peaks R(i+1)-R(i) and takes mean value
RR(1:length(R)-1)=0; %initiliase vector
for i=1:length(R)-1
RR(i)=R(i+1)-R(i);    
end
SD_RR=std(RR);
RR=ceil(median(RR));


end

