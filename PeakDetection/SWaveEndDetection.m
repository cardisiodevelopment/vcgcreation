function [ Swave_end ] = SWaveEndDetection( x5,S_loc )
% %Check end of S Wave
%looks at a squared slope (x5) which is lower than a given threshold,
%S_loc includes the locations of S peaks

Swave_end(1:length(S_loc))=0;
S_loc=S_loc+20*500/1000;
for i=1:length(S_loc)
    threshold_qrs=0.01;
    j=0;
    check_flat=0;
    while check_flat==0
        if j>30
            threshold_qrs=threshold_qrs*1.5;
            j=0;
        end
        
        lowerbound=S_loc(i)+j;
        upperbound=S_loc(i)+j+10;
        
        if upperbound>length(x5)
            Swave_end(i)=length(x5);
            break; 
        else
            check_flat=(sum(x5(lowerbound:upperbound))<threshold_qrs);
            j=j+1;
            if check_flat==1
                Swave_end(i)=S_loc(i)+j;
            end
        end
    end
end


end

