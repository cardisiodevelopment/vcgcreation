function [ T ] = TDetectionFromVCG(vcg,R,QRS_begin,QRS_end,RR)

vcg_noQRS=RemoveQRSFromSignal(vcg,QRS_begin,QRS_end,RR);
for i=1:length(R)-1
[T_value(i), T(i)] = max( vcg_noQRS(R(i):R(i+1)) );
T(i) = T(i)-1+R(i);
end
T=T(find(T~=0));
end

