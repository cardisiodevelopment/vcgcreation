function [ signal_without_QRS ] = RemoveQRSFromSignal( x1,Qwave_begin,Swave_end,RR )
%Removes QRS signal  from input signal x1. Start and End of QRS is given as
%well as RR peak distance. During QRS duration signal is set to zero and
%25% of RR Interval before begining of QRS complex (to omit false detection
%of P peaks during T detection)

QRSyesno(1:length(x1))=1;



for i=1:length(Qwave_begin)
n=ceil(RR/3); 
m=10;

 if Qwave_begin(i)-n <=0
   n=0;
end

if Swave_end(i)+m >= length(QRSyesno)
   m=0;
end

QRSyesno(Qwave_begin(i)-n:Swave_end(i)+m)=0;
end

QRSyesno=QRSyesno';
signal_without_QRS=QRSyesno.*x1;

end


