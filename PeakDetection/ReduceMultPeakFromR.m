function [ P_reduced ] = ReduceMultPeakFromR(R,P)
%Deletes the P/T peaks that are overrecognized, meaning 2 P/T peaks between
%distance of two R peaks is not possible.
%Start from last R, go back, make a check if one P/T is there and delete
%all other Ps/Ts which are in the same RR Interval

%remove Ps that are smaller than R(1) or bigger than R(last)
for i=1:length(P)
    if P(i)<R(1)||P(i)>R(length(R))
    P(i)=0; %sets entries to 0
    end
end

P_lim=P(P~=0); %removes entries that are 0, P_lim is the new array to work with

Pyesno(1:length(P_lim))=0; %Pyesno array, 1 or 0 to decide if P should be kept or deleted

%look at intervals R(i) R(i-1), the first P to come (from backwards) should
%be kept (Pyesno=1), all the others will be deleted (Pyesno=0)
for i=length(R):-1:2
    for j=length(P_lim):-1:1
        if P_lim(j)<R(i)&&P_lim(j)>R(i-1)
            Pyesno(j)=1;
            break;
        end
    end
end
P_reduced=Pyesno.*P_lim;

P_reduced=P_reduced(P_reduced~=0);
end

