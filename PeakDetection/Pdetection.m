function [P,x_der_sq] = Pdetection( x1,QRSx_begin,Tx_end )
N=length(x1);

x1_noQRST=RemoveTFromSignal(x1,QRSx_begin,Tx_end);

% Make impulse response
h = [-1 -2 0 2 1]/8;
% Apply filter Derivation
x_der = conv (x1 ,h);
x_der = x_der (2+[1: N]);

x_der=RemoveTFromSignal( x_der,QRSx_begin,Tx_end );
%x_der(1:ceil(N/10))=0;
%x_der(N-ceil(N/10):N)=0;
x_der = x_der/ max( abs(x_der ));

%Signal Squaring
x_sq = x_der .^2;
x_sq = x_sq/ max( abs(x_sq ));
x_der_sq=x_sq;

%Removing of QRS and Tsignal from data

%x_der_sq_noQRST=x_der_sq_noQRST/max(abs(x_der_sq_noQRST));
x_moving_average_noQRST=MovingAverage(x_der_sq);




%Find T Points Which it is different than Pan-Tompkins algorithm
x_qrs=x_moving_average_noQRST;

max_h = max(x_qrs);
thresh = mean (x_qrs );
poss_reg =(x_qrs>thresh*max_h)';
    


left = find(diff([0 poss_reg])==1); %finds entries with difference==1, meaning at this spot there is the jump form 0 to 1
right = find(diff([poss_reg 0])==-1);% finds entries with the jump from 1 to 0


P(1:length(left))=0; %initialisation of R,Q, S location and value
P_value(1:length(left))=0;


Pbegin_value(1:length(left))=0;
Pbegin(1:length(left))=0;
Pend_value(1:length(left))=0;
Pend(1:length(left))=0;

for i=1:length(left)

    [P_value(i), P(i)] = max( x1_noQRST(left(i):right(i)) ); %looks at window left(i)-right(i) e.g. entry 800:900, T goes from 1 to 100
    P(i) = P(i)-1+left(i); % add offset 

%     [Pbegin_value(i), Pbegin(i)] = min( x1(left(i):P(i)) );
%     Pbegin(i) = Pbegin(i)-1+left(i); % add offset
% 
%     [Pend_value(i), Pend(i)] = min( x1(P(i):right(i)) );
%     Pend(i) = Pend(i)-1+P(i); % add offset

end


% there is no selective wave
Pbegin=Pbegin(find(Pbegin~=0));
P=P(find(P~=0));
Pend=Pend(find(Pend~=0));







end

