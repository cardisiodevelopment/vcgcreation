function [ signal_without_QRST ] = RemoveTFromSignal( x1,Qwave_begin,T_end )
%Removes QRS signal  from input signal x1. Start and End of QRS is given as
%well as RR peak distance. During QRS duration signal is set to zero and
%25% of RR Interval before begining of QRS complex (to omit false detection
%of P peaks during T detection)

QRSyesno(1:length(x1))=1;
QRSyesno(1:min([Qwave_begin T_end]))=0; %everything before first Qwave/or Twave
QRSyesno(max([Qwave_begin T_end]):length(QRSyesno))=0; %everything after last Qwave / T wave set to 0


for i=1:min([length(Qwave_begin) length(T_end)])
    n=5;  %35 ms no Q after P
    m=5; %150ms to P wave after T wave

    if Qwave_begin(i)-n <=0
        n=0;
    end

    if T_end(i)+m >= length(QRSyesno)
        m=0;
    end

    if Qwave_begin(i)<T_end(i)
        QRSyesno(Qwave_begin(i)-n:T_end(i)+m)=0;
    else
        if i+1<=length(T_end)
            QRSyesno(Qwave_begin(i)-n:T_end(i+1)+m)=0;
        end
    end
end

QRSyesno=QRSyesno';
signal_without_QRST=QRSyesno.*x1;

end


