function  plotVCG_and_Intervals( t,x1,Rx,Tx,P,QRSx_begin,QRSx_end,Tx_begin,Tx_end,P_begin,P_end,fs)
%plots ECG lead with QRS points and beginning and end of QRS complex

  plot (t,x1,t(Rx),x1(Rx),'r^',t(Tx),x1(Tx),'b^',t(P),x1(P),'ks','LineWidth',2);

  zoom(gcf,'reset');
    axis([3 9 min(x1(3*fs:9*fs)) max(x1(3*fs:9*fs))]);
    for j=1:length(QRSx_begin)
        line([t(QRSx_begin(j)) t(QRSx_begin(j))],[min(x1)*2 x1(QRSx_begin(j))],'Color','r','LineWidth',2);
        line([t(QRSx_end(j)) t(QRSx_end(j))],[min(x1)*2 x1(QRSx_end(j))],'Color','r','LineWidth',2);
    end
    
    for j=1:length(Tx_end)
    line([t(Tx_begin(j)) t(Tx_begin(j))],[min(x1)*2 x1(Tx_begin(j))],'Color','b','LineWidth',2)    
    line([t(Tx_end(j)) t(Tx_end(j))],[min(x1)*2 x1(Tx_end(j))],'Color','b','LineWidth',2)
    end
    
    for j=1:length(P_end)
    line([t(P_begin(j)) t(P_begin(j))],[min(x1)*2 x1(P_begin(j))],'Color','k','LineWidth',2)    
    line([t(P_end(j)) t(P_end(j))],[min(x1)*2 x1(P_end(j))],'Color','k','LineWidth',2)
    end

end

