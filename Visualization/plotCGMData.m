function  plotCGMData( X1,Y1,Z1 )
%plots CGM vector loop

plot3(X1,Y1,Z1);
% Create xlabel
xlabel('X [mV]');

% Create ylabel
ylabel('Y [mV]');

% Create zlabel
zlabel('Z [mV]');

% Create title
title({'CGM'});


end

