function [] = plotECG(time,V,graphTitle,lineColor,fs)
%plots ECG signal with given visualization settings

% Create plot
plot(time,V,'LineWidth',1,'Color',lineColor);
%axis([3 5 min(V(3*fs:9*fs)) max(V(3*fs:9*fs))]);
axis([0 time(length(time)) -inf inf]);

% Create xlabel
xlabel('Time [s]');

% Create ylabel
ylabel('Amp [mV]');

% Create title
title({graphTitle});
grid('on');
end

