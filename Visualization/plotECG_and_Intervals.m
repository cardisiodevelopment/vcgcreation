function  plotECG_and_Intervals( t,x1,Rx,Sx,Qx,Tx,fs)
%plots ECG lead with QRS points and beginning and end of QRS complex

  plot (t,x1,t(Rx) ,x1(Rx) , 'r^', t(Sx) ,x1(Sx), '*',t(Qx) , x1(Qx), 'o',t(Tx),x1(Tx),'b^');
  zoom(gcf,'reset');
    axis([3 9 min(x1(3*fs:9*fs)) max(x1(3*fs:9*fs))]);

%     for j=1:length(QRSx_begin)
%         line([t(QRSx_begin(j)) t(QRSx_begin(j))],[min(x1) x1(QRSx_begin(j))],'Color','r','LineWidth',2);
%         line([t(QRSx_end(j)) t(QRSx_end(j))],[min(x1) x1(QRSx_end(j))],'Color','r','LineWidth',2);
%     end
%     
%     for j=1:length(Tx_end)
%     line([t(Tx_begin(j)) t(Tx_begin(j))],[min(x1) x1(Tx_begin(j))],'Color','b','LineWidth',2)    
%     line([t(Tx_end(j)) t(Tx_end(j))],[min(x1) x1(Tx_end(j))],'Color','b','LineWidth',2)
%     end
    
%     for j=1:length(P_end)
%     line([t(P_begin(j)) t(P_begin(j))],[min(x1) x1(P_begin(j))],'Color','k','LineWidth',2)    
%     line([t(P_end(j)) t(P_end(j))],[min(x1) x1(P_end(j))],'Color','k','LineWidth',2)
%     end

end

