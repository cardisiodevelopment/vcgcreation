function ExportCGMParamInOneTable(ExportFolderPath,CGMParameterMatrix,FileName_list)
%Plots computed CGM parameters to excel sheet,
%next to it the information from Sanz Parameter Diagnosis is plotted as
%well.%both is saved in ExcelSpread Sheet (Path)
%Input are all parameters as well as content of Sanz Diagnosis Table and
%Patient Name

names={'MDphi','SDphi','MDalphaR','SDalphaR','MDalphaT','SDalphaT','MDbetaR','SDbetaR','MDbetaT','SDbetaT','MDRoct4',...
    'SDRoct4','MDToct5','SDToct5','MDToct6','SDToct6','MeanToctP5','MDSumTbas','SDSumTbas','MDRatioPT','SDRatioPT',...
    'MDSumPMinus','SDSumPMinus','MDalphaRini','SDalphaRini','MeanRvmaxTvmax','MDRintTint','SDRintTint'};


%Definition of Entire Matrix
AllParameters=[names;num2cell(CGMParameterMatrix)]';

for i=1:length(FileName_list)
PatientNames{i}=FileName_list{i}(1:length(FileName_list{i})-4);
end
PatientNames=PatientNames;

%Write the data the Excel Sheet
filename = strcat(ExportFolderPath,'\','AllCGMParameters.xlsx');
xlswrite(filename,PatientNames,1,'B1')
xlswrite(filename,AllParameters,1,'A2')

end

